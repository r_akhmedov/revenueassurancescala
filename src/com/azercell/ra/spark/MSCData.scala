package com.azercell.ra.spark

import java.text.SimpleDateFormat

/**
 * Created by itrahim on 10/12/2015.
 */
case class MSCData(
  callType: String,             //0
service: String,                //1
chargedPartyMSISDN: String,     //2
chargedPartyIMSI: String,       //3
chargedPartyIMEI: String,       //4
otherPartyNumber: String,       //5
otherPartyTON: String,           //6
otherPartyNPI: String,           //7
thirdPartyNumber: String,       //8
thirdPartyTON: String,           //9
thirdPartyNPI: String,           //10
  callStartDate: Long,            //11
callDuration: Long,              //12
cellID: String,                 //13
originLocationNumber: String,   //14
teleServiceCode: String,        //15
bearerServiceCode: String,      //16
//supplementaryServCode: String, 17
//mSCExchangeID: String,        18
//mSCAddress: String,           19
//incomingRoute: String,        20
//outgoingRoute: String,        21
//sMSCAddress: String,            //22
//messageReference: String,       //23
calledPartyMNPAddrees: String   //24
//calledPartyMNPNPI		[26] IA5STRING OPTIONAL,
//eventVolume				[27] IA5STRING OPTIONAL,
//messageVolume			[28] IA5STRING OPTIONAL,
//casueForForward		[29] IA5STRING OPTIONAL
                    )


object MSCData {
  def apply(record: String) = {
    val fields = record.split(",", 29)
    new MSCData(
      fields(0),
      fields(1),
      fields(2),
      fields(3),
      fields(4),
      fields(5),
      fields(6),
      fields(7),
      fields(8),
      fields(9),
      fields(10),
      fields(11) match {
        case x if x.isEmpty() || x == null => 0
        case x => new SimpleDateFormat("yyyyMMddHHmmss").parse(x).getTime()
      },
      fields(12) match {
        case x if x.isEmpty() || x == null => 0
        case x => x.toLong
      },
      fields(13),
      fields(14),
      fields(15),
      fields(16),
      fields(24)
    )
  }
}
