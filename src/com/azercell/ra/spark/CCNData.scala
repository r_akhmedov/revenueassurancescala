package com.azercell.ra.spark

import java.text.SimpleDateFormat

/**
 * Created by itrahim on 9/23/2015.
 */
case class CCNData(
  msisdn:              String, //1
  sessionid:           String, //2
  serviceid:           String, //3
  timeunits:           Long,   //4
  bytes:               Long,   //5
  eventunits:          Long,   //6
  timeid:              Long,   //7
  serviceSenario:      String, //8
  roamPosition:        String, //9
  serviceClass:        String, //10
  accountValueBefore:  Double, //11
  accountValueAfter:   Double, //12
  finalCharge:         Double, //13
  cell:                String, //14
  ratingGroup:         String, //15
  locno:               String, //16
  opNumber:            String, //17
  recordID:            Int, //28
  partialID:           Int,    //29
  isLastPart:          Boolean, //30
  sequenceID:          Int, //31
  sequenceIDsums:      Int
) {

  def merge(ccndata2: CCNData): CCNData  = {
    if (Some(ccndata2).isEmpty) this
    else {
      if (!this.getKey.equals(ccndata2.getKey)) throw new KeyMissmatchException
      CCNData(this.msisdn, this.sessionid, this.serviceid, this.timeunits + ccndata2.timeunits, this.bytes + ccndata2.bytes,
        this.eventunits + ccndata2.eventunits,
        if (this.timeid > ccndata2.timeid) ccndata2.timeid else this.timeid,
        this.serviceSenario, this.roamPosition, this.serviceClass,
        this.accountValueBefore match {
          case x if x >= ccndata2.accountValueBefore => this.accountValueBefore
          case x => ccndata2.accountValueBefore
        },
        this.accountValueAfter match {
          case x if x <= ccndata2.accountValueAfter => this.accountValueBefore
          case x => ccndata2.accountValueAfter
        },
        this.finalCharge + ccndata2.finalCharge, this.cell, this.ratingGroup, this.locno, this.opNumber, this.recordID,
        if (ccndata2.partialID > this.partialID) ccndata2.partialID else this.partialID,
        if (ccndata2.isLastPart) ccndata2.isLastPart else this.isLastPart,
        if (ccndata2.sequenceID > this.sequenceID) ccndata2.sequenceID else this.sequenceID,
        this.sequenceID + ccndata2.sequenceID)
    }
  }

  def getKey = {
    val serviceid = this.serviceid match {
      case "voice" => "0"
      case "sms" => "4"
      case "diameter" => "3"
      case "mms" => "6"
      case "data" => "8"
    }
    Utils.compressString(s"${this.sessionid}$serviceid${this.msisdn.substring(3)}")
  }

  def getSession = s"${this.sessionid},${this.serviceid},${this.msisdn}"

  def isCompleteRecord = (sequenceID*(sequenceID+1)/2 == sequenceIDsums) && isLastPart


}


object CCNData {
  private def safeToInt(x: String): Int = x match {
    case x if x == null || x.isEmpty => 0
    case x => x.toInt
  }

  private  def safeToDouble(x: String): Double = x match {
    case x if x == null || x.isEmpty => 0.0
    case x => x.toDouble
  }

  def apply(record: String): CCNData = {
    val fields = record.split("\\,", 33)
    val sessionid: String = fields(0) match {
      case x if x.startsWith("Ericsson_OCS") => fields(19)
      case x if x.startsWith("SCAP") => fields(3)
      case x => fields(1)
    }
    val service: String = fields(4) match {
      case x if x == null || x.isEmpty() => "none"
      case x => Integer.parseInt(x) match {
        case 4 => "sms"
        case 3 => "diameter"
        case 6 => "mms"
        case y if y >= 1000 => "data"
        case _ => "voice"
      }
    }

    var roampos: String = ""
    val timestamp: Long = fields(8) match {
      case x if x == null || x.isEmpty => 0
      case x => new SimpleDateFormat("yyyyMMddHHmmss").parse(x.substring(0, 14)).getTime()
    }

    if(service.equals("sms") && fields(0).startsWith("SCAP"))
      roampos = "r"
    else if(service.equals("data") && !fields(26).equals("40001") && fields(26).length() >=5)
      roampos = "r"
    else roampos = fields(10)

    new CCNData(fields(2), // 1
      sessionid,    //2
      service,  //3
      fields(5) match {           //4
        case null => 0
        case x if x.isEmpty => 0
        case x => x.toLong
      },
      fields(6) match {     //5
        case null => 0
        case x if x.isEmpty => 0
        case x => x.toLong
      },
      fields(7) match {     //6
        case null => 0
        case x if x.isEmpty => 0
        case x => x.toLong
      },
      timestamp, //7
      fields(9), //8
      roampos,    //9
      fields(12), //10
      safeToDouble(fields(13)),
      safeToDouble(fields(14)),
      safeToDouble(fields(16)),
      fields(21), //15
      fields(22), // 16
      fields(23), //17
      fields(24) match {
        case null => null
        case x if x.isEmpty => null
        case x if x.length > 2 => x.substring(2)
        case x => null
      },
      safeToInt(fields(28)),
      safeToInt(fields(29)),
      fields(30) match {
        case "0" => false
        case x => true
      },
      safeToInt(fields(31)),
      safeToInt(fields(31))
    )

  }




}




