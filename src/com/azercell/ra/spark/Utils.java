package com.azercell.ra.spark;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 * Created by itrahim on 10/20/2015.
 */
public class Utils {

    public static int getPosSig(String str) {
        int posSig=0;
        for(char ch: str.toCharArray()) {
            posSig = posSig << 1;
            if (ch != '0' && ch != '1' && ch != '2' && ch != '3' &&
                ch != '4' && ch != '5' && ch != '6' && ch != '7' &&
                ch != '8' && ch != '9' && ch != 'A' && ch != 'B' &&
                ch != 'C' && ch != 'D' && ch != 'E' && ch != 'F' )
                posSig ++;
        }
        return posSig;
    }

    public static String compressString(String str) throws DecoderException
    {

        int posSig = 0; // position signature if hexdec then 0 esle 1
        StringBuilder sb = new StringBuilder();
        String [] tokensHex = str.toUpperCase().split("[^0-9A-F]");
        String [] tokensStr = str.toUpperCase().split("[0-9A-F]");

        for(String token: tokensStr) {
            sb.append(token);
        }

        byte [] byteBuff = new byte[tokensHex.length];

        for(String token: tokensHex) {
            if (token.length() % 2 == 1)
                token += "0";
            sb.append(new String(Hex.decodeHex(token.toCharArray())));
        }

        return sb.toString();

    }

    public static String compressStringWithSig(String str) throws DecoderException
    {
        return Utils.compressString(str) + Utils.getPosSig(str);
    }



}
