package com.azercell.ra.spark

import java.net.URI
import oracle.jdbc.pool.OracleDataSource
import org.apache.spark.rdd.RDD
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{Path, FileSystem}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.flume.FlumeUtils
import org.apache.spark.streaming.{Minutes, StreamingContext}
import org.apache.spark.SparkConf

/**
 * Created by itrahim on 9/28/2015.
 */
object ApplicationMain {


  def insertData(oraConn: String, oraTable: String, interval: Int, rdd: RDD[(Long, (Long, Int, Long, Int))]) = {
    rdd.foreachPartition(p => {
      val ods = new OracleDataSource()
      ods.setURL(oraConn)
      val conn =  ods.getConnection()
      conn.setAutoCommit(true)
      val stmt = conn.prepareStatement(s"insert into $oraTable (inDate, chkInterval, chkDate, mscDuration, mscCount, ccnDuration, ccnCount) values (sysdate, ?, ?, ?, ?, ?, ?)")
      p.foreach(t => {
        stmt.setInt(1, interval)
        stmt.setDate(2, new java.sql.Date(t._1))
        stmt.setLong(3, t._2._1)
        stmt.setInt(4, t._2._2)
        stmt.setLong(5, t._2._3)
        stmt.setInt(6, t._2._4)
        stmt.execute()
      })
      stmt.close()
      conn.close()
    })
  }

  def createStreamContext(confFile: String): StreamingContext = {
    val conf = new Configuration()
    val fs = FileSystem.newInstance(conf)
    conf.addResource(fs.open(new Path(URI.create(confFile).getPath)))
    conf.reloadConfiguration()

    /*
    com.azercell.ra.spark.duration     streaming batch duration
    com.azercell.ra.spark.checkpoint   streaming check point
    com.azercell.ra.ccn.flume.port     flume port
    com.azercell.ra.ccn.flume.host     flume host
    com.azercell.ra.msc.flume.port     flume port
    com.azercell.ra.msc.flume.host     flume host
    com.azercell.ra.ccn.maxwaitdur     maximum wait time in minutes, for record consolidation
    com.azercell.ra.ccn.waitdur        wait time in minutes, for record consolidation
    com.azercell.ra.checkIntervals      check intervals in minutes
    com.azercell.ra.ora.connection     jdbc connection string
    com.azercell.ra.ora.table          oracle table
    */

    val batchDuration = conf.getInt("com.azercell.ra.spark.duration", -1)
    val ccnFlumeHost = conf.get("com.azercell.ra.ccn.flume.host")
    val ccnFlumePort = conf.getInt("com.azercell.ra.ccn.flume.port", -1)
    val checkpointDir = conf.get("com.azercell.ra.spark.checkpoint")

    // version.2
    val checkIntervals = conf.get("com.azercell.ra.checkIntervals")
    val oraConnStr     = conf.get("com.azercell.ra.ora.connection")
    val oraTable       = conf.get("com.azercell.ra.ora.table")

    val sc = new SparkConf()
    sc.setAppName("RevenueAssuranceChecks")
    val ssc = new StreamingContext(sc, Minutes(batchDuration))
    ssc.checkpoint(checkpointDir)

    val inputEvents = FlumeUtils.createPollingStream(ssc, ccnFlumeHost, ccnFlumePort).
      map( e => new String(e.event.getBody.array()) )

    val ccnVoiceEvents = inputEvents.
      filter(e => (e.length() - e.replace(",", "").length()) == 32 ).
      map(CCNData(_)).
      filter( ccndata => ccndata.roamPosition == "h" && ccndata.serviceid =="voice" ).
      map( c => (c.timeid-(c.timeid % (batchDuration*60000)), (c.timeunits, Set(c.getSession))) ).
      reduceByKey( (a, b) => (a._1+b._1, a._2 union b._2) ).
      persist(StorageLevel.MEMORY_AND_DISK_SER_2)

    val mscVoiceEvents = inputEvents.
      filter( e => (e.length() - e.replace(",", "").length()) == 29 ).
      map(MSCData(_)).
      filter( mscdata => mscdata.service == "voice" &&  !mscdata.chargedPartyIMSI.isEmpty() && mscdata.chargedPartyIMSI.matches("40001..[13].*")).
      map( m => (m.callStartDate-(m.callStartDate % (batchDuration*60000)), (m.callDuration, 1) ) ).
      reduceByKey( (a,b) => (a._1 + b._1, a._2 + b._2) ).
      persist(StorageLevel.MEMORY_AND_DISK_SER_2)

    (mscVoiceEvents fullOuterJoin  ccnVoiceEvents).
      map( kv => (kv._1, (kv._2._1.getOrElse((0l,0))._1, //msc voice call duration
                          kv._2._1.getOrElse((0,0))._2, //msc voice call count
                          kv._2._2.getOrElse((0l,Set()))._1,  //ccn voice call duration
                          kv._2._2.getOrElse((0,Set()))._2.size //ccn voice call count
                          ) ) ).
      foreachRDD( insertData(oraConnStr, oraTable, batchDuration, _ ))

    checkIntervals.split(",").foreach( i => {
      val interval = i.toInt
      val ccnVoiceGroup = ccnVoiceEvents.
        window(Minutes(interval), Minutes(interval)).
        map(c => ( c._1-(c._1 % (interval*60000)), c._2 )).
        reduceByKey( (a, b) => (a._1+b._1, a._2 union b._2) )

      val mscVoiceGroup = mscVoiceEvents.
        window(Minutes(interval), Minutes(interval)).
        map(c => ( c._1-(c._1 % (interval*60000)), c._2 )).
        reduceByKey( (a,b) => (a._1 + b._1, a._2 + b._2) )

      (mscVoiceGroup fullOuterJoin ccnVoiceGroup).
        map( kv => (kv._1, (kv._2._1.getOrElse((0l,0))._1,
                            kv._2._1.getOrElse((0,0))._2,
                            kv._2._2.getOrElse((0l,Set()))._1,
                            kv._2._2.getOrElse((0,Set()))._2.size))).
        foreachRDD( insertData(oraConnStr, oraTable, interval, _ ))
    })

    ssc

  }

  def main (args: Array[String]): Unit = {

    val conf = new Configuration()
    val fs = FileSystem.newInstance(conf)
    conf.addResource(fs.open(new Path(URI.create(args(0)).getPath)))
    conf.reloadConfiguration()

    val checkpointDir = conf.get("com.azercell.ra.spark.checkpoint")

    val ssc = StreamingContext.getOrCreate(checkpointDir, () => createStreamContext(args(0))  )
    ssc.start()
    ssc.awaitTermination()
  }
}
