import java.text.SimpleDateFormat
import java.util.Date
import com.azercell.ra.spark.CCNData
import org.apache.commons.codec.binary.Hex

/*
val ccncdr = CCNData("Ericsson_OCS_V1_0.0.0.7.32251@3gpp.org,100:1000:1,994509961908,BAKM109-0.sasntoccn4azercell.com;"+
  "1443607379;878830244,1000,,500,,20150930150334+0500,mo,h,,44,0.123322,0.123322,,0.000000,,,2163815527,internet,06CC,"+
  "100,,,0,40001,8657050221617101,1542699732,0,0,4,")

val ccncdr2 = CCNData("Core_Context_R1A@ericsson.com,8-1642930701-0d22ed61,994507142859,,0,16,,,20150930152447+0500,mo,"+
  "h,,23,1.858315,1.808315,,0.050000,,,0,,,0,99450940801710099450,1800994504156947,1101,,,1542699734,0,,1,")

val ccncdr3 = CCNData("Core_Context_R1A@ericsson.com,8-1642930701-0d22ed61,994507142859,,0,16,,,20150930152427+0500,mo,"+
  "h,,23,1.808315,1.758315,,0.050000,,,0,,,0,99450940801710099450,1800994504156947,1101,,,1542699734,0,,1,")

(ccncdr2 merge ccncdr3)

val date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())

val ccndrs: Seq[CCNData] = Seq(CCNData("Core_Context_R1A@ericsson.com,8-1642930701-0d22ed61,994507142859,,0,16,,,20150930152427+0500,mo,"+
  "h,,23,1.808315,1.758315,,0.050000,,,0,,,0,99450940801710099450,1800994504156947,1101,,,1542699734,0,,1,"),
  CCNData("Core_Context_R1A@ericsson.com,8-1642930701-0d22ed61,994507142859,,0,16,,,20150930152447+0500,mo,"+
    "h,,23,1.858315,1.808315,,0.050000,,,0,,,0,99450940801710099450,1800994504156947,1101,,,1542699734,0,,3,")
)

ccndrs.reduce(_.merge(_)).isCompleteRecord

val str = "Core_Context_R1A@ericsson.com,8-1642930701-0d22ed61,994507142859,,0,16,,,20150930152427+0500,mo,"+
  "h,,23,1.808315,1.758315,,0.050000,,,0,,,0,99450940801710099450,1800994504156947,1101,,,1542699734,0,,1,"


*/

import com.azercell.ra.spark.Utils

val str = Utils.compressString("502210436xyz35357878")

str.length


val a = 1


val ccncdr3 = CCNData("Core_Context_R1A@ericsson.com,8-1642930701-0d22ed61,994507142859,,0,16,,,20150930152427+0500,mo,"+
  "h,,23,1.808315,1.758315,,0.050000,,,0,,,0,99450940801710099450,1800994504156947,1101,,,1542699734,0,,1,")

ccncdr3.getKey

ccncdr3.getKey.length

Utils.getPosSig(ccncdr3.getKey)

Utils.compressString(ccncdr3.getKey)

Utils.compressString(ccncdr3.getKey).length

Int.MaxValue
Long.MaxValue